l = []

while True:
    inp = input(" Enter number ")
    if inp in "exit":
        break

    l.append(int(inp))

print(l)

import math


def fn(x):
    return (x ** 2) / (x ** 3 - 2 * math.pi)


new_list = [fn(x) for x in l]
print(new_list)
